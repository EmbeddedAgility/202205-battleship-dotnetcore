﻿
namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Battleship.GameController;
    using Battleship.GameController.Contracts;

    public class Program
    {
        private static List<Ship> myFleet = new List<Ship>();

        private static List<Ship> enemyFleet = new List<Ship>();

        private static ConsoleColor PlayerColor = ConsoleColor.Blue;
        private static ConsoleColor EnemyColor = ConsoleColor.Cyan;
        private static ConsoleColor SuccessColor = ConsoleColor.Green;
        private static ConsoleColor ErrorColor = ConsoleColor.Red;
        private static ConsoleColor WarningColor = ConsoleColor.Yellow;

        static void Main()
        {
            Console.Title = "Battleship";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("                                     |__");
            Console.WriteLine(@"                                     |\/");
            Console.WriteLine("                                     ---");
            Console.WriteLine("                                     / | [");
            Console.WriteLine("                              !      | |||");
            Console.WriteLine("                            _/|     _/|-++'");
            Console.WriteLine("                        +  +--|    |--|--|_ |-");
            Console.WriteLine(@"                     { /|__|  |/\__|  |--- |||__/");
            Console.WriteLine(@"                    +---------------___[}-_===_.'____                 /\");
            Console.WriteLine(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            Console.WriteLine(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            Console.Write(@"|                        ");
            ConsoleWriteInColor("Welcome to Battleship", ConsoleColor.Yellow);
            Console.WriteLine("                         BB-61/");
            Console.WriteLine(@" \_________________________________________________________________________|");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;

            string input;
            do
            {
                Console.WriteLine("Type \"S\" to start or \"X\" to exit the game...");
                input = Console.ReadLine();

                if (input == "S")
                {
                    Play();
                }
                else if (input == "X")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Please enter valid input...");
                }
            }
            while (input != "S" || input != "X");
        }

        private static void Play()
        {
            myFleet.Clear();
            enemyFleet.Clear();

            InitializeGame();

            StartGame();
        }

        private static void StartGame()
        {
            Console.Clear();
            Console.WriteLine("                  __");
            Console.WriteLine(@"                 /  \");
            Console.WriteLine("           .-.  |    |");
            Console.WriteLine(@"   *    _.-'  \  \__/");
            Console.WriteLine(@"    \.-'       \");
            Console.WriteLine("   /          _/");
            Console.WriteLine(@"  |      _  /""");
            Console.WriteLine(@"  |     /_\'");
            Console.WriteLine(@"   \    \_/");
            Console.WriteLine(@"    """"""""");

            do
            {
                Console.WriteLine("==================================");
                ConsoleWriteInColor("Player", PlayerColor);
                Console.WriteLine(", it's your turn");
                Console.WriteLine("Enter coordinates for your shot :");
                var position = ParsePosition(Console.ReadLine());

                var enemyShip = GameController.GetShipAtPosition(enemyFleet, position);

                if (enemyShip != null)
                {
                    var enemyShipIsHit = GameController.CheckIsHit(enemyShip, position);

                    if (enemyShipIsHit)
                    {
                        enemyShip.MarkPostionAsHit(position);

                        if (enemyShipIsHit)
                        {
                            Console.Beep();

                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine(@"                \         .  ./");
                            Console.WriteLine(@"              \      .:"";'.:..""   /");
                            Console.WriteLine(@"                  (M^^.^~~:.'"").");
                            Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                            Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                            Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                            Console.WriteLine(@"                 -\  \     /  /-");
                            Console.WriteLine(@"                   \  \   /  /");
                            Console.ForegroundColor = ConsoleColor.White;
                            ConsoleWriteLineInColor("Yeah ! Nice hit !", SuccessColor);

                            if (enemyShip.IsDestroyed())
                            {
                                ConsoleWriteLineInColor($"Enemy ship {enemyShip.Name} was destroyed !", SuccessColor);
                            }

                            if (GameController.IsFleetDestroyed(enemyFleet))
                            {
                                ConsoleWriteLineInColor("You win !", SuccessColor);

                                Console.WriteLine("Type \"R\" to play again or anything else to exit the game !");
                                var input = Console.ReadLine();
                                if (input == "R")
                                {
                                    Play();
                                }
                                else
                                {
                                    Environment.Exit(0);
                                }
                            }
                        }
                    }
                    else
                    {
                        ConsoleWriteLineInColor("Already hit !", WarningColor);
                    }
                }
                else
                {
                    ConsoleWriteLineInColor("Miss !", ErrorColor);
                }

                Console.WriteLine("==================================");
                ConsoleWriteInColor("Enemy's", EnemyColor);
                Console.WriteLine(" turn");

                var playerShipPosition = GetRandomPosition();
                var playerShip = GameController.GetShipAtPosition(enemyFleet, playerShipPosition);

                if (playerShip != null)
                {
                    var playerShipIsHit = GameController.CheckIsHit(playerShip, playerShipPosition);

                    if (playerShipIsHit)
                    {
                        playerShip.MarkPostionAsHit(playerShipPosition);

                        if (playerShipIsHit)
                        {
                            Console.Beep();

                            Console.WriteLine(@"                \         .  ./");
                            Console.WriteLine(@"              \      .:"";'.:..""   /");
                            Console.WriteLine(@"                  (M^^.^~~:.'"").");
                            Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                            Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                            Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                            Console.WriteLine(@"                 -\  \     /  /-");
                            Console.WriteLine(@"                   \  \   /  /");
                        }

                        ConsoleWriteLineInColor($"Enemy shot in {playerShipPosition.Column}{playerShipPosition.Row} and has hit your ship !", ErrorColor);

                        if (playerShip.IsDestroyed())
                        {
                            ConsoleWriteLineInColor($"Player ship {enemyShip.Name} was destroyed !", ErrorColor);
                        }
                    }
                    else
                    {
                        ConsoleWriteLineInColor($"Enemy shot in {playerShipPosition.Column}{playerShipPosition.Row} but it was already hit !", WarningColor);
                    }
                }
                else
                {
                    ConsoleWriteLineInColor($"Enemy shot in {playerShipPosition.Column}{playerShipPosition.Row} and missed !", SuccessColor);
                }
            }
            while (true);
        }

        public static Position ParsePosition(string input)
        {
            var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
            var number = int.Parse(input.Substring(1, 1));
            return new Position(letter, number);
        }

        private static Position GetRandomPosition()
        {
            int rows = 8;
            int lines = 8;
            var random = new Random();
            var letter = (Letters)random.Next(lines);
            var number = random.Next(rows);
            var position = new Position(letter, number);
            return position;
        }

        private static void InitializeGame()
        {
            InitializeMyFleet();

            InitializeEnemyFleet();
        }

        private static void InitializeMyFleet()
        {
            InitializePlayerFleet();
        }

        private static void InitializePlayerFleetFromInput()
        {
            myFleet = GameController.InitializeShips().ToList();

            Console.WriteLine("Please position your fleet (Game board size is from A to H and 1 to 8) :");

            foreach (var ship in myFleet)
            {
                Console.WriteLine();
                Console.WriteLine("Please enter the positions for the {0} (size: {1})", ship.Name, ship.Size);
                for (var i = 1; i <= ship.Size; i++)
                {
                    Console.WriteLine("Enter position {0} of {1} (i.e A3):", i, ship.Size);
                    ship.AddPosition(Console.ReadLine());
                }
            }
        }

        private static void InitializePlayerFleet()
        {
            myFleet = GameController.InitializeShips().ToList();

            myFleet[0].Positions.Add(new Position { Column = Letters.G, Row = 1 });
            myFleet[0].Positions.Add(new Position { Column = Letters.H, Row = 1 });
            myFleet[0].Positions.Add(new Position { Column = Letters.F, Row = 1 });

            myFleet[1].Positions.Add(new Position { Column = Letters.B, Row = 2 });
            myFleet[1].Positions.Add(new Position { Column = Letters.B, Row = 3 });

            //myFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 4 });
            //myFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 5 });
            //myFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 6 });
            //myFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 7 });
            //myFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 8 });

            //myFleet[1].Positions.Add(new Position { Column = Letters.H, Row = 6 });
            //myFleet[1].Positions.Add(new Position { Column = Letters.H, Row = 7 });
            //myFleet[1].Positions.Add(new Position { Column = Letters.H, Row = 8 });
            //myFleet[1].Positions.Add(new Position { Column = Letters.H, Row = 9 });

            //myFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 6 });
            //myFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 6 });
            //myFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 6 });

            //myFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 1 });
            //myFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 1 });
            //myFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 1 });

            //myFleet[4].Positions.Add(new Position { Column = Letters.B, Row = 2 });
            //myFleet[4].Positions.Add(new Position { Column = Letters.B, Row = 3 });
        }

        private static void InitializeEnemyFleet()
        {
            enemyFleet = GameController.InitializeShips().ToList();

            enemyFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 8 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 8 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.C, Row = 8 });

            enemyFleet[1].Positions.Add(new Position { Column = Letters.F, Row = 5 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.F, Row = 6 });

            //enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            //enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            //enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 6 });
            //enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 7 });
            //enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 8 });

            //enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            //enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 7 });
            //enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 8 });
            //enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 9 });

            //enemyFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 3 });
            //enemyFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 3 });
            //enemyFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });

            //enemyFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 8 });
            //enemyFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 8 });
            //enemyFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 8 });

            //enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 5 });
            //enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 6 });
        }

        public static void ConsoleWriteLineInColor(string text, ConsoleColor color)
        {
            var defaultColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = defaultColor;
        }

        public static void ConsoleWriteInColor(string text, ConsoleColor color)
        {
            var defaultColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = defaultColor;
        }
    }
}
